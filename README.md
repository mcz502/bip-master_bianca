# bip-master_bianca
# Scripts to run FSL BIANCA within BB pipeline

**bianca_labels**: <br />
- To be included in 'data' directory (in /bip) <br />
- Contains the files needed to run classifier <br />

**FileTree.tree**: <br />
- To be included in 'data' directory (in /bip) <br />
- Updated with the output files (filenames) when running FSL bianca <br />

**struct_T2_FLAIR.py**: <br />
- To be included in 'struct_T2_FLAIR' directory (in /bip/pipelines) <br />
- Updated with the lines to run the script for FSL bianca <br />

**T2_FLAIR_bianca.py**: <br />
- To be included in 'struct_T2_FLAIR' directory (in /bip/pipelines) <br />
- Runs FSL bianca using the outputs from T2 & T2_FLAIR directories <br />









