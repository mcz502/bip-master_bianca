#!/usr/bin/env python

import os
import logging
from shutil import copyfile
from pipe_tree import In, Out, Ref
from fsl import wrappers
from bip.utils import redirect_logging
from fsl.wrappers.bianca import (bianca,bianca_cluster_stats,bianca_overlap_measures,bianca_perivent_deep,make_bianca_mask)

log = logging.getLogger(__name__)

def run(ctx,
        T1_unbiased:                           In,
        T2_FLAIR_unbiased:                     In,
        T1_to_MNI_warp_coef_inv:               In,
        T1_to_MNI_linear_mat:                  In,
        T1_fast_pve_0:                         In,
        T1_unbiased_brain:                     In,
        logs_dir:                              Ref,
        T1_dir:                                Ref,
        T2_FLAIR_dir:                          Ref,
        lesion_dir:                            Ref,
        T1_transforms_dir:                     Ref,
        T2_FLAIR_bianca_conf_file:             Out,
        T1_unbiased_bianca_mask:               Out,
        T1_unbiased_ventmask:                  Out,
        T2_FLAIR_bianca_mask:                  Out,
        T2_FLAIR_bianca_final_mask:            Out,
        T2_FLAIR_BIANCA_deepwm_map:            Out,
        T2_FLAIR_BIANCA_perivent_map:          Out,
        T2_FLAIR_BIANCA_tot_pvent_deep_volume: Out,
        T2_FLAIR_BIANCA_volume:                Out
        ):
    
    with redirect_logging('T1_bianca', outdir=logs_dir):
        
        # Create bianca mask
        wrappers.make_bianca_mask(T1_unbiased, T1_fast_pve_0, T1_to_MNI_warp_coef_inv, keep_files=False)
        
        # Copy the files to T2_FLAIR_dir
        T1_unbiased_bianca_mask_tmp = T1_dir + '/T1_unbiased_bianca_mask.nii.gz'
        T1_unbiased_ventmask_tmp    = T1_dir + '/T1_unbiased_ventmask.nii.gz'
        T1_unbiased_brain_mask_tmp  = T1_dir + '/T1_unbiased_brain_mask.nii.gz'       
        copyfile(src=T1_unbiased_bianca_mask_tmp, dst=lesion_dir + '/T1_unbiased_bianca_mask.nii.gz')
        copyfile(src=T1_unbiased_ventmask_tmp,    dst=lesion_dir + '/T1_unbiased_ventmask.nii.gz')
        copyfile(src=T1_unbiased_brain_mask_tmp,  dst=lesion_dir + '/T1_unbiased_brain_mask.nii.gz')
        
        # Create masterfile for bianca
        filenames =[T1_dir            + '/T1_unbiased_brain.nii.gz',
                    T2_FLAIR_dir      + '/T2_FLAIR_unbiased.nii.gz',
                    T1_transforms_dir + '/T1_to_MNI_linear.mat']                                   
        with open(T2_FLAIR_bianca_conf_file,'w') as f:
            for j in filenames:
                f.write(j+" ")
        
        # Run bianca
        wrappers.bianca(singlefile          = T2_FLAIR_bianca_conf_file, 
                        querysubjectnum     = 1, 
                        brainmaskfeaturenum = 1,
                        loadclassifierdata  = ctx.get_data("bianca_data/bianca_class_data"),
                        matfeaturenum       = 3,
                        featuresubset       = "1,2",
                        o                   = T2_FLAIR_bianca_mask)  
        
        # Multiply the lesions mask (bianca_mask) by the "unbiased_bianca_mask" 
        wrappers.fslmaths(T2_FLAIR_bianca_mask).mul(T1_unbiased_bianca_mask).thr(0.8).bin().run(T2_FLAIR_bianca_final_mask)
        
         # Calculate total WMH (volume of the final mask), perventricular WMH & deep WMH - 10mm Criteria  
        wrappers.bianca_perivent_deep(wmh_map        = T2_FLAIR_bianca_final_mask,
                                      vent_mask      = T1_unbiased_ventmask,
                                      minclustersize = 0,
                                      outputdir      = lesion_dir,
                                      do_stats       = 2)
        copyfile(src=T2_FLAIR_BIANCA_tot_pvent_deep_volume, dst=T2_FLAIR_BIANCA_volume)
