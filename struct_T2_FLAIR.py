 #!/usr/bin/env python

import logging
from bip.utils import redirect_logging
from bip.pipelines.struct_T2_FLAIR import T2_FLAIR_gdc, T2_FLAIR_brain_extract 
from bip.pipelines.struct_T2_FLAIR import T2_FLAIR_defacing, T2_FLAIR_apply_bfc
from bip.pipelines.struct_T2_FLAIR import T2_FLAIR_bianca

log = logging.getLogger(__name__)

def add_to_pipeline(ctx, pipe, tree, targets):

    logs_dir=tree.get('logs_dir')

    with redirect_logging('pipe_struct_T2_FLAIR', outdir=logs_dir):
        pipe(T2_FLAIR_gdc.run, submit=dict(jobtime=200), kwargs={'ctx' : ctx})
        targets.append('T2_FLAIR_orig_ud_warp')
        pipe(T2_FLAIR_brain_extract.run, submit=dict(jobtime=200), kwargs={'ctx' : ctx})
        targets.append('T2_FLAIR_brain')
        pipe(T2_FLAIR_defacing.run, submit=dict(jobtime=200), kwargs={'ctx' : ctx})
        targets.append('T2_FLAIR_defacing_mask')
        pipe(T2_FLAIR_apply_bfc.run, submit=dict(jobtime=200), kwargs={'ctx' : ctx})
        targets.append('T2_FLAIR_unbiased_brain')
        pipe(T2_FLAIR_bianca.run, submit=dict(jobtime=200), kwargs={'ctx' : ctx})
        targets.append('T2_FLAIR_BIANCA_volume')
    return pipe, targets
